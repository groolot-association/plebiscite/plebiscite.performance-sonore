:: Script de lancement des éléments nécessaires
:: pour la performance 'Plébiscite'
@title Plebiscite launcher
@echo "Lancement de la performance Plebiscite"
psexec -d "C:\Program Files\Jack\jackd.exe" -R -S -d net
psexec -d "C:\Program Files\MIDIOX\midiox.exe" "D:\creation\plebiscite\plebiscite_midiox_profile.ini"
::psexec -d "C:\QLC+\qlcplus.exe" -k -o D:\creation\lives-concerts\lights.qxw
psexec -d "C:\Program Files\pd\bin\pd.exe" -noaudio -midiindev 3 -path "D:\creation\plebisicte" -open "D:\creation\plebiscite\VIDEO_direct_from_bcf.pd"
psexec -d "C:\Program Files\Image-Line\FL Studio 11\FL.exe" "D:\creation\plebiscite\plebiscite_live_pommes_sauvages.flp"
@exit 0
