Sound pack downloaded from Freesound.org
----------------------------------------

This pack of sounds contains sounds by xserra ( http://www.freesound.org/people/xserra/  )
You can find this pack online at: http://www.freesound.org/people/xserra/packs/7862/


License details
---------------

Sampling+: http://creativecommons.org/licenses/sampling+/1.0/
Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/
Attribution: http://creativecommons.org/licenses/by/3.0/
Attribution Noncommercial: http://creativecommons.org/licenses/by-nc/3.0/


Sounds in this pack
-------------------

  * 162204__xserra__sufi-5.wav
    * url: http://www.freesound.org/people/xserra/sounds/162204/
    * license: Attribution
  * 162203__xserra__sufi-6.wav
    * url: http://www.freesound.org/people/xserra/sounds/162203/
    * license: Attribution
  * 162202__xserra__sufi-7.wav
    * url: http://www.freesound.org/people/xserra/sounds/162202/
    * license: Attribution
  * 162201__xserra__sufi-8.wav
    * url: http://www.freesound.org/people/xserra/sounds/162201/
    * license: Attribution
  * 162200__xserra__sufi-1.wav
    * url: http://www.freesound.org/people/xserra/sounds/162200/
    * license: Attribution
  * 162199__xserra__sufi-2.wav
    * url: http://www.freesound.org/people/xserra/sounds/162199/
    * license: Attribution
  * 162198__xserra__sufi-3.wav
    * url: http://www.freesound.org/people/xserra/sounds/162198/
    * license: Attribution
  * 162197__xserra__sufi-4.wav
    * url: http://www.freesound.org/people/xserra/sounds/162197/
    * license: Attribution
  * 161712__xserra__turkish-music-4.wav
    * url: http://www.freesound.org/people/xserra/sounds/161712/
    * license: Attribution
  * 161711__xserra__turkish-music-6.wav
    * url: http://www.freesound.org/people/xserra/sounds/161711/
    * license: Attribution
  * 161709__xserra__turkish-music-5.wav
    * url: http://www.freesound.org/people/xserra/sounds/161709/
    * license: Attribution
  * 161708__xserra__turkish-music-3.wav
    * url: http://www.freesound.org/people/xserra/sounds/161708/
    * license: Attribution
  * 161707__xserra__turkish-music-1.wav
    * url: http://www.freesound.org/people/xserra/sounds/161707/
    * license: Attribution
  * 161706__xserra__turkish-music-2.wav
    * url: http://www.freesound.org/people/xserra/sounds/161706/
    * license: Attribution
  * 126100__xserra__Selcuk_Sipahioglu_2.wav
    * url: http://www.freesound.org/people/xserra/sounds/126100/
    * license: Attribution
  * 126099__xserra__Selcuk_Sipahioglu_1.wav
    * url: http://www.freesound.org/people/xserra/sounds/126099/
    * license: Attribution
  * 126098__xserra__Asim_Tokel.wav
    * url: http://www.freesound.org/people/xserra/sounds/126098/
    * license: Attribution
  * 126097__xserra__Istanbul_Sazendeleri_8.wav
    * url: http://www.freesound.org/people/xserra/sounds/126097/
    * license: Attribution
  * 126096__xserra__Istanbul_Sazendeleri_7.wav
    * url: http://www.freesound.org/people/xserra/sounds/126096/
    * license: Attribution
  * 126095__xserra__Istanbul_Sazendeleri_6.wav
    * url: http://www.freesound.org/people/xserra/sounds/126095/
    * license: Attribution
  * 126094__xserra__Istanbul_Sazendeleri_5.wav
    * url: http://www.freesound.org/people/xserra/sounds/126094/
    * license: Attribution
  * 126093__xserra__Istanbul_Sazendeleri_4.wav
    * url: http://www.freesound.org/people/xserra/sounds/126093/
    * license: Attribution
  * 126092__xserra__Istanbul_Sazendeleri_3.wav
    * url: http://www.freesound.org/people/xserra/sounds/126092/
    * license: Attribution
  * 126091__xserra__Istanbul_Sazendeleri_2.wav
    * url: http://www.freesound.org/people/xserra/sounds/126091/
    * license: Attribution
  * 126090__xserra__Istanbul_Sazendeleri_1.wav
    * url: http://www.freesound.org/people/xserra/sounds/126090/
    * license: Attribution
  * 116053__xserra__enprovaze_3.wav
    * url: http://www.freesound.org/people/xserra/sounds/116053/
    * license: Attribution
  * 116052__xserra__enprovaze_2.wav
    * url: http://www.freesound.org/people/xserra/sounds/116052/
    * license: Attribution
  * 116051__xserra__enprovaze_1.wav
    * url: http://www.freesound.org/people/xserra/sounds/116051/
    * license: Attribution
  * 116050__xserra__concert_3.wav
    * url: http://www.freesound.org/people/xserra/sounds/116050/
    * license: Attribution
  * 116049__xserra__concert_2.wav
    * url: http://www.freesound.org/people/xserra/sounds/116049/
    * license: Attribution
  * 116048__xserra__concert_1.wav
    * url: http://www.freesound.org/people/xserra/sounds/116048/
    * license: Attribution
  * 115701__xserra__fasil_music_3.wav
    * url: http://www.freesound.org/people/xserra/sounds/115701/
    * license: Attribution
  * 115700__xserra__fasil_music_2.wav
    * url: http://www.freesound.org/people/xserra/sounds/115700/
    * license: Attribution
  * 115699__xserra__fasil_music_1.wav
    * url: http://www.freesound.org/people/xserra/sounds/115699/
    * license: Attribution
  * 115615__xserra__Yavuz_Akalin_Ney.wav
    * url: http://www.freesound.org/people/xserra/sounds/115615/
    * license: Attribution
  * 115614__xserra__Segah_Ney.wav
    * url: http://www.freesound.org/people/xserra/sounds/115614/
    * license: Attribution
  * 115401__xserra__yayli_tanbur.wav
    * url: http://www.freesound.org/people/xserra/sounds/115401/
    * license: Attribution
  * 115400__xserra__tanbur.wav
    * url: http://www.freesound.org/people/xserra/sounds/115400/
    * license: Attribution
  * 115399__xserra__oud.wav
    * url: http://www.freesound.org/people/xserra/sounds/115399/
    * license: Attribution
  * 115398__xserra__ney.wav
    * url: http://www.freesound.org/people/xserra/sounds/115398/
    * license: Attribution
  * 115397__xserra__kudum.wav
    * url: http://www.freesound.org/people/xserra/sounds/115397/
    * license: Attribution
  * 115396__xserra__darbuka.wav
    * url: http://www.freesound.org/people/xserra/sounds/115396/
    * license: Attribution
  * 115395__xserra__cumbus_2.wav
    * url: http://www.freesound.org/people/xserra/sounds/115395/
    * license: Attribution
  * 115394__xserra__cumbus_1.wav
    * url: http://www.freesound.org/people/xserra/sounds/115394/
    * license: Attribution
  * 115392__xserra__bendir.wav
    * url: http://www.freesound.org/people/xserra/sounds/115392/
    * license: Attribution
  * 115227__xserra__turkish_baglama.wav
    * url: http://www.freesound.org/people/xserra/sounds/115227/
    * license: Attribution
  * 115226__xserra__oud.wav
    * url: http://www.freesound.org/people/xserra/sounds/115226/
    * license: Attribution
  * 115224__xserra__Ruhi_Ayangil_Kanun_2.wav
    * url: http://www.freesound.org/people/xserra/sounds/115224/
    * license: Attribution
  * 115223__xserra__Ruhi_Ayangil_Kanun_1.wav
    * url: http://www.freesound.org/people/xserra/sounds/115223/
    * license: Attribution
  * 115222__xserra__Ney.wav
    * url: http://www.freesound.org/people/xserra/sounds/115222/
    * license: Attribution

