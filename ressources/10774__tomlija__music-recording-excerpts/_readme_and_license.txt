Sound pack downloaded from Freesound.org
----------------------------------------

This pack of sounds contains sounds by Tomlija ( http://www.freesound.org/people/Tomlija/  )
You can find this pack online at: http://www.freesound.org/people/Tomlija/packs/10774/


License details
---------------

Sampling+: http://creativecommons.org/licenses/sampling+/1.0/
Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/
Attribution: http://creativecommons.org/licenses/by/3.0/
Attribution Noncommercial: http://creativecommons.org/licenses/by-nc/3.0/


Sounds in this pack
-------------------

  * 110334__Tomlija__Traditional_Eastern_instrument_Sargija_improvisation_played_by_Boris_Todorovic.aif
    * url: http://www.freesound.org/people/Tomlija/sounds/110334/
    * license: Attribution
  * 110333__Tomlija__Traditional_Eastern_instrument_Macedonian_Tambura_improvisation_played_by_Boris_Todorovic.aif
    * url: http://www.freesound.org/people/Tomlija/sounds/110333/
    * license: Attribution
  * 110332__Tomlija__Traditional_Eastern_instrument_Kaval_improvisation_played_by_Boris_Todorovic.aif
    * url: http://www.freesound.org/people/Tomlija/sounds/110332/
    * license: Attribution

