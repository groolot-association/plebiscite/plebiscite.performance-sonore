Sound pack downloaded from Freesound.org
----------------------------------------

This pack of sounds contains sounds by batchku ( http://www.freesound.org/people/batchku/  )
You can find this pack online at: http://www.freesound.org/people/batchku/packs/581/


License details
---------------

Sampling+: http://creativecommons.org/licenses/sampling+/1.0/
Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/
Attribution: http://creativecommons.org/licenses/by/3.0/
Attribution Noncommercial: http://creativecommons.org/licenses/by-nc/3.0/


Sounds in this pack
-------------------

  * 10676__batchku__sant_roll_A_3.aif
    * url: http://www.freesound.org/people/batchku/sounds/10676/
    * license: Creative Commons 0
  * 10677__batchku__sant_roll_A_5.aif
    * url: http://www.freesound.org/people/batchku/sounds/10677/
    * license: Creative Commons 0
  * 10675__batchku__sant_roll_G_5.aif
    * url: http://www.freesound.org/people/batchku/sounds/10675/
    * license: Creative Commons 0
  * 10674__batchku__sant_roll_G_4.aif
    * url: http://www.freesound.org/people/batchku/sounds/10674/
    * license: Creative Commons 0
  * 10672__batchku__sant_roll_F4.aif
    * url: http://www.freesound.org/people/batchku/sounds/10672/
    * license: Creative Commons 0
  * 10673__batchku__sant_roll_D5.aif
    * url: http://www.freesound.org/people/batchku/sounds/10673/
    * license: Creative Commons 0
  * 10670__batchku__sant_roll_D_4.aif
    * url: http://www.freesound.org/people/batchku/sounds/10670/
    * license: Creative Commons 0
  * 10667__batchku__sant_roll_D_5.aif
    * url: http://www.freesound.org/people/batchku/sounds/10667/
    * license: Creative Commons 0
  * 10668__batchku__sant_roll_F5.aif
    * url: http://www.freesound.org/people/batchku/sounds/10668/
    * license: Creative Commons 0
  * 10671__batchku__sant_roll_G5.aif
    * url: http://www.freesound.org/people/batchku/sounds/10671/
    * license: Creative Commons 0
  * 10666__batchku__sant_roll_D4.aif
    * url: http://www.freesound.org/people/batchku/sounds/10666/
    * license: Creative Commons 0
  * 10669__batchku__sant_roll_C4.aif
    * url: http://www.freesound.org/people/batchku/sounds/10669/
    * license: Creative Commons 0
  * 10665__batchku__sant_roll_C5.aif
    * url: http://www.freesound.org/people/batchku/sounds/10665/
    * license: Creative Commons 0
  * 10664__batchku__sant_roll_A_4.aif
    * url: http://www.freesound.org/people/batchku/sounds/10664/
    * license: Creative Commons 0
  * 10663__batchku__sant_roll_G4.aif
    * url: http://www.freesound.org/people/batchku/sounds/10663/
    * license: Creative Commons 0

