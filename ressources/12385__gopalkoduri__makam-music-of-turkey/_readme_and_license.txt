Sound pack downloaded from Freesound.org
----------------------------------------

This pack of sounds contains sounds by gopalkoduri ( http://www.freesound.org/people/gopalkoduri/  )
You can find this pack online at: http://www.freesound.org/people/gopalkoduri/packs/12385/


License details
---------------

Sampling+: http://creativecommons.org/licenses/sampling+/1.0/
Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/
Attribution: http://creativecommons.org/licenses/by/3.0/
Attribution Noncommercial: http://creativecommons.org/licenses/by-nc/3.0/


Sounds in this pack
-------------------

  * 195088__gopalkoduri__arzuhalim-sana-ey-kas-keman-havada-turna-sesi-gelir.wav
    * url: http://www.freesound.org/people/gopalkoduri/sounds/195088/
    * license: Attribution Noncommercial
  * 195087__gopalkoduri__eridi-kalmad-daglarn-kar.wav
    * url: http://www.freesound.org/people/gopalkoduri/sounds/195087/
    * license: Attribution Noncommercial
  * 195086__gopalkoduri__tercan-kazasndan-gelen-bir-gelin.wav
    * url: http://www.freesound.org/people/gopalkoduri/sounds/195086/
    * license: Attribution Noncommercial
  * 195085__gopalkoduri__efsaneyim-performed-using-one-variety-of-mzrap-right-hand-technique.wav
    * url: http://www.freesound.org/people/gopalkoduri/sounds/195085/
    * license: Attribution Noncommercial
  * 195084__gopalkoduri__efsaneyim-performed-using-one-variety-of-mzrap-right-hand-technique.wav
    * url: http://www.freesound.org/people/gopalkoduri/sounds/195084/
    * license: Attribution Noncommercial
  * 195083__gopalkoduri__efsaneyim-performed-using-selpe-right-hand-technique.wav
    * url: http://www.freesound.org/people/gopalkoduri/sounds/195083/
    * license: Attribution Noncommercial
  * 195082__gopalkoduri__incecikten-bir-kar-yagar-kemence-performance.wav
    * url: http://www.freesound.org/people/gopalkoduri/sounds/195082/
    * license: Attribution Noncommercial
  * 195081__gopalkoduri__tebim-teravetim-gel-tar-performance.wav
    * url: http://www.freesound.org/people/gopalkoduri/sounds/195081/
    * license: Attribution Noncommercial
  * 195080__gopalkoduri__eziz-dostum-tar-performance.wav
    * url: http://www.freesound.org/people/gopalkoduri/sounds/195080/
    * license: Attribution Noncommercial
  * 195078__gopalkoduri__huseyni-saz-semaisi-ney-performance.wav
    * url: http://www.freesound.org/people/gopalkoduri/sounds/195078/
    * license: Attribution Noncommercial

